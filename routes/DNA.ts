import { Router } from 'express';
import { dna } from '../controllers/DNA';

const router = Router();

router.post('/', dna);

export default router;