import { Router } from 'express';
import { stats } from '../controllers/STATS';

const router = Router();

router.get('/stats', stats);

export default router;