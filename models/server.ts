import express, { Application } from 'express';
import DNA_Routes from '../routes/DNA';
import STATS_Routes from '../routes/STATS';
import test_Routes from '../routes/test';
import cors from 'cors';
import { dbConnection } from '../db/connection';

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json'));

class Server {

    private app: Application;
    private port: string ;
    private apiPaths = {
        DNA: '/mutation',
        STATS: '/',
        TEST: '/api',
    }

    constructor() {
        this.app = express();
        this.port = config.PORT || '8000';

        this.dbConnection();
        this.middlewares();
        this.routes();
    }

    // TODO: Conectar a base de datos
    async dbConnection() {
        try {
            await dbConnection();
            
        } catch (error) {
            // throw new Error( error );
            console.log(error);
        }
    }

    middlewares() {
        // CORS
        this.app.use( cors() );
        this.app.use( express.json({ limit: '50mb' }) );
        this.app.use( express.static('public') );
    }

    routes() {
        this.app.use( this.apiPaths.DNA, DNA_Routes );
        this.app.use( this.apiPaths.STATS, STATS_Routes );
        this.app.use( this.apiPaths.TEST, test_Routes );
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        });
    }
}

export default Server;