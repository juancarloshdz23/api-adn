import { Schema, model } from 'mongoose';

const DNA = new Schema({
    dna : {
        type: String,
        allowNull: false,
        required: true,
        unique: true
    },
    mutation : {
        type: Boolean,
        default: false
    }
});

export default model('DNA', DNA);