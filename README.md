# API DETECCIÓN DE ADN

Los servicios que se presentan en está api te permiten detectar si una persona tiene diferencias genéticas basándose en su secuencia de ADN y obtener estadísticas del ADN verificado.

# Clonación del repositorio
1. Necesitaremos 'git' para la descarga, si no lo tienes instalado puedes realizarlo desde [ruta git](https://git-scm.com/downloads)
2. Abrir la terminal en la ruta que desea para guardar el proyecto.
3. Ejecuta el comando `git clone https://gitlab.com/juancarloshdz23/api-adn.git`

# Instalación local
1. Entrar al folder donde tenemos el proyecto.
2. Tener la versión 14.x.x instalado en tu equipo. (Preferible 14.16.0) En caso contrario puedes revisar [tutorial](https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-windows)
3. Ejecuta `npm install` para obtener los 'node_modules'.
4. Modifica el nombre del archivo '.config.json' a 'config.json', seguido de eso abre el archivo y colocar en la variable '"MONGODB_CNN"' tu cadena a la base de datos de MongoDB. Si no cuentas con una cadena de conexión puedes generar una de manera gratuita en la siguiente [ruta mongodb](https://www.mongodb.com/). Por default se encuentra en el puerto 3000, si desea cambiar el puerto modifica la variable '"PORT"'.
5. Compila la versión de typescript `tsc`, esto generara una carpeta denominada dist, la cual nos permitirá ejecutar el api.
6. Ejecuta el comando `node dist/app.js` para correr el api y realiza un test a la [ruta prueba api](http://localhost:3000/api/test). Si te aparece el mensaje de confirmación el api se encuentra ejecutando de forma 'local' de manera correcta.

# Probar los servicios
1. El api está conformada por los siguientes 2 servicios:

1.1 - Un servicio POST para confirmar si una secuencia de ADN tiene una mutación, el servicio puede ser consumido con el siguiente formato:

**POST** → /mutation/

{
        “dna”:["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}

1.2 - Otro servicio que te permitirá revisar las estadísticas de verificaciones de ADN, que puede ser consumido de la siguiente forma:

**GET** → /stats




# API DNA DETECTION

The services presented in this API allow you to detect if a person has genetic differences based on their DNA sequence and obtain statistics from the verified DNA.

# Cloning the repository
1. We will need 'git' for the download, if you don't have it installed you can do it from [git path](https://git-scm.com/downloads)
2. Open the terminal in the path you want to save the project.
3. Run the command `git clone https://gitlab.com/juancarloshdz23/api-adn.git`

# Local installation
1. Enter the folder where we have the project.
2. Have version 14.x.x installed on your computer. (Preferable 14.16.0) Otherwise you can review [tutorial](https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-windows)
3. Run `npm install` to get the 'node_modules'.
4. Change the name of the '.config.json' file to 'config.json', followed by that open the file and place in the variable '"MONGODB_CNN"' your string to the MongoDB database. If you don't have a connection string you can generate one for free at the following [mongodb path](https://www.mongodb.com/). By default it is in port 3000, if you want to change the port, modify the variable '"PORT"'.
5. Compile the `tsc` version of typescript, this will generate a folder called dist, which will allow us to run the api.
6. Run the `node dist/app.js` command to run the api and test the [api test path](http://localhost:3000/api/test). If the confirmation message appears, the api is running 'locally' correctly.

# Test services
1. The api is made up of the following 2 services:

1.1 - A POST service to confirm if a DNA sequence has a mutation, the service can be consumed with the following format:

**POST** → /mutation/

{
         “dna”:["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}

1.2 - Another service that will allow you to review the statistics of DNA verifications, which can be consumed in the following format:

**GET** → /stats
