import { Request, Response } from 'express';
import DNA  from '../models/DNA';

export const dna = async( req: Request, res: Response ) => {

    const { dna } = req.body;
    let mutation = false;

    try {
        if ( !isNitrogenDNA( dna ) ) {
            res.status(403).json({
                msg: 'Sólo se pueden recibir bases nitrogenadas válidas'
            });
        }

        mutation = hasMutation( dna );

        saveDna( dna, mutation );

    } catch (error) {
        res.status(403).json({
            msg: 'Hable con el administrador del sistema',
            error
        });
    }
    res.status(200).json(mutation);
};

const isNitrogenDNA = ( dnas: string[] ): boolean => {
    const lettersValid = ['A','T','C','G'];
    let valid = false;
    dnas.forEach( dna => {
        for ( let char of dna ) {
            lettersValid.includes(char) ? valid = true : valid = false;
        }
    });
    return valid;
};

const hasMutation = ( dnas: string[] ): boolean => {
    let mutation = false;
    let countDiagonal = 1, countOblique = 1, countVertical = 1, countHorizontal = 1;
    
    let headerDiagonal = '', lastDiagonal = '';
    let headerOblique = '', lastOblique = '';
    let headerVertical = '', lastVertical = '';
    let headerHorizontal = '', lastHorizontal = '';
    
    dnas.forEach((dna1, index1) => {
        dnas.forEach((dna2, index2) => {
            // Diagonal
            if ( index1 === index2 ) {
                if ( headerDiagonal === '' ) {
                    headerDiagonal = dna2[index2];
                } else if ( lastDiagonal === '' ) {
                    lastDiagonal = headerDiagonal;
                    headerDiagonal = dna2[index2];

                    if ( headerDiagonal === lastDiagonal ) {
                        countDiagonal += 1;
                    }
                } else if ( lastDiagonal !== '' ) {
                    lastDiagonal = headerDiagonal;
                    headerDiagonal = dna2[index2];

                    if ( headerDiagonal === lastDiagonal ) {
                        countDiagonal += 1;
                    } else {
                        countDiagonal = 1;
                        lastDiagonal = '';
                    }
                }
            }

            // Inverse
            if ( index2 === (dna1.length - (index1 + 1))) {
                const oblique = dna1[(dna1.length - (index1 + 1))];
                if ( headerOblique === '' ) {
                    headerOblique = oblique;
                } else if ( lastOblique === '' ) {
                    lastOblique = headerOblique;
                    headerOblique = oblique;

                    if ( headerOblique === lastOblique ) {
                        countOblique += 1;
                    } else {
                        countOblique = 1;
                    }
                } else if ( lastOblique !== '' ) {
                    lastOblique = headerOblique;
                    headerOblique = oblique;

                    if ( headerOblique === lastOblique ) {
                        countOblique += 1;
                    } else {
                        countOblique = 1;
                        lastOblique = '';
                    }
                }
            }

            // Vertical
            if ( index2 == 0 ) {
                headerVertical = '', lastVertical = '';
                countVertical = 1
            }

            if ( headerVertical === '' ) {
                headerVertical = dna1[index2];
            } else if ( lastVertical === '' ) {
                lastVertical = headerVertical;
                headerVertical = dna1[index2];

                if ( headerVertical === lastVertical ) {
                    countVertical += 1;
                }
            } else if ( lastVertical !== '' ) {
                lastVertical = headerVertical;
                headerVertical = dna1[index2];

                if ( headerVertical === lastVertical ) {
                    countVertical += 1;
                } else {
                    countVertical = 1;
                    lastVertical = '';
                }
            }

            // Horizontal
            if ( headerHorizontal === '' ) {
                headerHorizontal = dna2[index1];
            } else if ( lastHorizontal === '' ) {
                lastHorizontal = headerHorizontal;
                headerHorizontal = dna2[index1];

                if ( headerHorizontal === lastHorizontal ) {
                    countHorizontal += 1;
                }
            } else if ( lastHorizontal !== '' ) {
                lastHorizontal = headerHorizontal;
                headerHorizontal = dna2[index1];

                if ( headerHorizontal === lastHorizontal ) {
                    countHorizontal += 1;
                } else {
                    countHorizontal = 1;
                    lastHorizontal = '';
                }
            }

            if ( countDiagonal === 4 || 
                 countOblique  === 4 || 
                 countVertical === 4 || 
                 countHorizontal === 4 ) {
                mutation = true;
            }
        });
    });

    return mutation;
};

const saveDna = async( dna: string[], mutation: boolean ): Promise<void> => {
    
    const dnaString = JSON.stringify( dna );

    const saveDna = new DNA({ dna: dnaString, mutation });
    await saveDna.save();
};