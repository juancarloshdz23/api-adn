import { Request, Response } from 'express';
import DNA  from '../models/DNA';

export const stats = async( req: Request, res: Response ) => {

    let count_mutations = 0, count_no_mutation = 0, ratio = 0;

    try {
        [count_mutations, count_no_mutation] = await Promise.all([
            DNA.countDocuments({ mutation: true }),
            DNA.countDocuments({ mutation: false })
        ]);

        ratio = count_mutations/count_no_mutation;

    } catch (error) {
        res.status(403).json({
            msg: 'Hable con el administrador del sistema',
            error
        });
    }
    res.status(200).json({
        count_mutations,
        count_no_mutation,
        ratio
    });
};