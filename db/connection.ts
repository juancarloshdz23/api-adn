import mongoose from 'mongoose';

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json'));

export const dbConnection = async() => {
    try {
        mongoose.set('strictQuery', false);
        await mongoose.connect( config.PROD.MONGODB_CNN)
            .then(db => console.log('Base de datos online'));

    } catch (error) {
        console.log(error);
        throw new Error('Error a la hora de iniciar la base de datos');
    }
};